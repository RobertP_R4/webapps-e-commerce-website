<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title></title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Link to index specific style sheets -->
        <link rel="stylesheet" href="/CSS/index.css">
        <link rel="stylesheet" href="/CSS/Common.css">
    </head>
    <body>
    <!-- Bounding box contains all elements on the page -->
    <div id="login">
        <!-- Contains the logo and customer management text, set up this way so flex direction collumn would result in a desirable layout -->
            <div id="LoginHeader">
                <!-- Company logo set in css as background image property hence no image tags -->
                <div id="companyLogo"></div>
                <div id="loginText">Customer Management System</div>
            </div>
            <!-- Form used to capture user input and eventually send it to the server for comparison -->
            <form class="loginForm">
            <input type="text" id="Username" name="username" placeholder="Username">
            <input type="password" id="Password" name="password" placeholder="Password">
            <input type="submit" id="submit" value="LOGIN">
            </form>
        </div>
        <!-- Temporary link to get to page 2 as the submit button isn't set up to do that -->
        <a href="missionControl.php">temp link</a>
    </body>
</html>