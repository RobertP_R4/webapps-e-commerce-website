<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title></title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Link to mission Control specific style sheets -->
        <link rel="stylesheet" href="/CSS/missionControl.css">
        <link rel="stylesheet" href="/CSS/Common.css">
    </head>
    <body>
        <!-- creates a top bar div which will house a logo user account information and a logout button -->
        <div id="topBar">
            <!-- Logo styled using background image css hence no img tag-->
            <div id="logo"></div>
            <div id="details">
                <div id="name">JS555</div>
                <div id ="accountType">Admin</div>
            </div>
            <!-- Link back to index serving as a logout button -->
            <a href="index.php" id="Logout">LOGOUT</a>
        </div>
        <!-- Provides background for all the options below -->
        <div id="boundBox">
            <!-- first option product Management  -->
            <div id="productManagement">Product Management</div>
            <div id="productManagementContent">
                <!-- divs responsible for displaying elements that can represent product creation -->
                <div id="addProduct">Add Product</div>
                <form class="create">
                <input type="text" class="productCreation" name="title" placeholder="Enter Game Title">
                <input type="text" class="productCreation" name="price" placeholder="Enter Price">
                <input type="text" class="productCreation" name="stock" placeholder="Enter Stock">
                <input type="text" class="productCreation" name="rating" placeholder="Enter Pegi Rating">
                <input type="text" class="productCreation" name="platform" placeholder="Enter Platform">
                <input type="text" class="productCreation" name="genres" placeholder="Enter Genres">
                <input type="text" class="productCreation" name="keywords" placeholder="Enter Keywords">
                <input type="text" class="productCreation" name="Tlink" placeholder="Enter Thumbnail Link">
                <input type="text" class="productCreation" name="description" placeholder="Enter Description">
                <input type="submit" id="createProduct" value="Add game to database">
                </form>
                <!-- divs responsible for displaying elements that can represent product deletion -->
                <div id="removeProduct">Remove Product</div>
                <form class="search">
                <input type="text" id="search" name="search" placeholder="enter product name">
                <input type="submit" id="submit" value="Search">
                </form>
                <!-- Divs showing simulated results and where they would appear in the event of a real search -->
                <div id="result">
                <div class="sampleCustomer">Sample Product 1</div>
                <div class="delete2">Delete</div>
                </div>
                <div id="result">
                <div class="sampleCustomer">Sample Product 2</div>
                <div class="delete2">Delete</div>
                </div>
            </div>
            <!-- second option order management -->
            <div id="orderManagement">Order Management</div>
            <!-- Divs display pending orders and give the user an option to complete or delete them -->
            <div id="orderManagementContent">
            <!-- Final product will display all order information instead of a placeholder value -->
            <div class="order">
                <div class="sampleOrder">Sample Order 1</div>
                <div class="complete">Complete</div>
                <div class="delete">Delete</div>
            </div>
            <div class="order">
                <div class="sampleOrder">Sample Order 2</div>
                <div class="complete">Complete</div>
                <div class="delete">Delete</div>
            </div>
            <div class="order">
                <div class="sampleOrder">Sample Order 3</div>
                <div class="complete">Complete</div>
                <div class="delete">Delete</div>
            </div>
            </div>
            <!-- third option customer management -->
            <div id="customerManagement">Customer Management</div>
            <div id="customerManagementContent">
            <!-- divs allowing the user to search the customer by name and having the option to delete their data -->
            <div id="deleteCustomer">Delete Customer</div>
            <form class="search">
            <input type="text" id="search" name="search" placeholder="enter customer name">
            <input type="submit" id="submit" value="Search">
            </form>
            <!-- Customers in the final submission will have their details such as DOB email address and username displayed to avoid deleting the wrong user-->
            <div id="result">
                <div class="sampleCustomer">Sample Customer 1</div>
                <div class="delete2">Delete</div>
            </div>
            <div id="result">
                <div class="sampleCustomer">Sample Customer 2</div>
                <div class="delete2">Delete</div>
            </div>
            </div>
            <!-- fourth option employee management -->
            <div id="employeeManagement">Employee Management</div>
            <!-- divs that allow for searching for and deleting an employee -->
            <div id="deleteEmployee">Delete Employeee</div>
            <div id="employeeManagementContent">
            <form class="search">
            <input type="text" id="search" name="search" placeholder="enter employee name">
            <input type="submit" id="submit" value="Search">
            </form>
            <!-- Sample employee data showing where the search results would appear and what options are available to the admin -->
            <div id="result">
                <div class="sampleCustomer">Sample Employee 1</div>
                <div class="delete2">Delete</div>
            </div>
            <div id="result">
                <div class="sampleCustomer">Sample Employee 2</div>
                <div class="delete2">Delete</div>
            </div>
            <!-- Employee creation divs supply all the fields neccasary to create an employee in the database -->
            <div id="createEmployee">Create Employeee</div>
            <form class="create">
            <input type="text" class="employeeCreation" name="efname" placeholder="enter employee name">
            <input type="text" class="employeeCreation" name="elname" placeholder="enter employee last name">
            <input type="text" class="employeeCreation" name="euname" placeholder="enter employee user name">
            <input type="text" class="employeeCreation" name="epass" placeholder="enter employee password">
            <input type="submit" id="submitEmployee" value="Create new Account">
            </form>
            </div>
            <!-- Please note the cms represents an admin user, a regular employee will not have access employee and customer management services -->
    </body>
</html>