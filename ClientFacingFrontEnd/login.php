<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Products Details</title>
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@300;400;500;600;700&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
</head>
<body>
    <div class="header">
    <div class="container">
    <div class="navbar">
    <div class="logo">
    <img src="images/logo.png" alt="" width="50px">                
</div>
<nav>
    <ul id="menuitem">
    <li><a href="index.php">Home</a></li>
        <li><a href="login.php">Login</a></li>
        <li><a href="register.php">Register</a></li>
        <li><a href="account.php"> My Account</a></li>
        <li><a href="#">Logout</a></li>
        <li><a href="cart.php">Basket</a></li>
</ul>
</nav>
</div>
</div>
    <div class="col-md-9">
<!--        WELCOME STRIP-->
	<div class="row">
	<div class="box">
	    <h1>Customer Login</h1>
		<p>
        <a href="register.php" class="btn btn-success btn-sm">Click here to REGISTER.</a>
		</p>
	</div>
	</div>

            <!-- ITEMS-->
            <div class="row">
			<div class="box">
            <form style="border:1px solid #ccc" method='POST' > <!--add POST Action -->
			<div class="container">
				
			<br>
			<label for="email"><b>Username/Email</b></label><br>
			<input type="email" placeholder="Enter Email" name="email" required><br>

			<label for="password"><b>Password</b></label><br>
			<input type="password" placeholder="Enter Password" name="password" required><br>
				
			<div class="clearfix">
			<button type="submit" class="signupbtn">Login</button>
	</div>
	</div>
	</form>  
	</div>
    </div>
    </div>
    </div>
    </div>
</html>