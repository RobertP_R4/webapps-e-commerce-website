<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ecommerce Website</title>
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<body>
    <div class="header">                                             <!--heading format-->
    <div class="container">
    <div class="navbar">
    <div class="logo">
    <img src="./images/logo.png" alt="" width="100px">                
    </div>
    <nav>
       <!-- The form -->
       <form class="example" action="action_page.php">
        <input type="text" placeholder="Search products.." name="search">
        <button type="submit"><i class="fa fa-search"></i></button>
    </form>
    <ul id="menuitem">
        <li><a href="index.php">Home</a></li>                        <!--links between different pages in the website-->
        <li><a href="login.php">Login</a></li>
        <li><a href="register.php">Register</a></li>
        <li><a href="account.php"> My Account</a></li>
        <li><a href="#">Logout</a></li>
        <li><a href="cart.php">Basket</a></li>
    </ul>
    </nav>
    </div>
        <div class="row">
        <div class="col-2">
        <h2> Want to buy gaming accessories?</h2><br>
        <p>Shop Now</p>
        <a href="" class="btn">Explore now &#8594;	</a>
    </div>
    </div>
    <!-- --------- categories--------- -->
        <div class="categories">
        <div class="small-container">
        <h2> Featured Products</h2>
        <div class="row">
        <div class="col-3">
        <img src="images/playstation.jpg">
    </div>
        <div class="col-3">
        <img src="images/gaming.png">
    </div>
        <div class="col-3">
        <img src="images/samsung.jpg">
    </div>
    </div>
    </div>
    </div>
 <!-- --------- product--------- -->
        <div class="small-container">
        <div class="row row-2">
    <select>
        <option value="">Default Sorting</option>               <!--Item Sorting options-->
        <option value="">Sort by Price</option>
        <option value="">Sort by Ranking</option>
        <option value="">Sort by Popular</option>
        <option value="">Sort by Sale</option>
    </select>
    </div>
    <!-- --------- product--------- -->
        <div class="small-container">
        <h2 class="title">All Products</h2>
        <div class="row">
        <div class="col-4">
        <a href="#"><img src="images/playstation.jpg"></a>
        <a href="#"><h4>Playstation 5</h4></a>
        <div class="rating">
            <i class="fa fa-star"></i>
            <i class="fa fa-star"></i>
            <i class="fa fa-star"></i>
            <i class="fa fa-star"></i>
            <i class="fa fa-star-o"></i>
    </div>
            <p>£750.00</p>                                   <!-- Price tag for product-->
    </div>
            <div class="col-4">
            <img src="images/gaming.png">
            <h4>ASUS Gaming Laptop</h4>
            <div class="rating">
                <i class="fa fa-star"></i>
                <i class="fa fa-star"></i>
                <i class="fa fa-star"></i>
                <i class="fa fa-star-half-o"></i>
                <i class="fa fa-star-o"></i>
    </div>
                <p>£1000.00</p>
    </div>
            <div class="col-4">
            <img src="images/xbox.jpg">
            <h4>Microsoft Xbox Series X 1TB</h4>
            <div class="rating">               1            <!--displays rating for products-->
                <i class="fa fa-star"></i>
                <i class="fa fa-star"></i>
                <i class="fa fa-star"></i>
                <i class="fa fa-star"></i>
                <i class="fa fa-star-half-o"></i>
                </div>
                <p>£999.00</p>
    </div>
            <div class="col-4">
            <img src="images/camera.jpg">                    <!--Products display with picture-->
            <h4>DSLR Camera</h4>
            <div class="rating">
                <i class="fa fa-star"></i>
                <i class="fa fa-star"></i>
                <i class="fa fa-star"></i>
                <i class="fa fa-star"></i>
                <i class="fa fa-star-o"></i>
    </div>
            <p>£575.00</p>
            </div>
    </div>
            <h2 class="title"></h2>
            <div class="row">
            <div class="col-4">
            <img src="images/mac.jpg">
            <h4>Macbook</h4>
            <div class="rating">
                <i class="fa fa-star"></i>
                <i class="fa fa-star"></i>
                <i class="fa fa-star"></i>
                <i class="fa fa-star"></i>
                <i class="fa fa-star-o"></i>
                </div>
                <p>£1055.00</p>
    </div>
                <div class="col-4">
                <img src="images/samsung.jpg">
                <h4>Samsung LED Tv</h4>
                <div class="rating">
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star-half-o"></i>
                    <i class="fa fa-star-o"></i>
                </div>
                <p>£450.00</p>
            
    </div>  
    
        <div class="page-btn">                              <!--generates next page buttons-->
            <span>1</span>
            <span>2</span>
            <span>3</span>
            <span>4</span>
            <span>&#8594;</span>
    </div>
    </div>  
</body>
</html>