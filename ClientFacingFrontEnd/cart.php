<!DOCTYPE html>
<html lang="en">
<head>
    <title>Cart</title>
	<link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
    <link rel="stylesheet" href="./css/style.css">
</head>
<body>
        <div class="header">
        <div class="container">
        <div class="navbar">
        <div class="logo">
        <img src="images/logo.png" alt="" width="50px">                
</div>
<nav>
        <ul id="menuitem">
        <li><a href="index.php">Home</a></li>
        <li><a href="login.php">Login</a></li>
        <li><a href="register.php">Register</a></li>
        <li><a href="account.php"> My Account</a></li>
        <li><a href="#">Logout</a></li>
        <li><a href="cart.php">Basket</a></li>
</ul>
</nav>
</div>
</div>
        <h1> Shopping Cart </h1>
        <div id="table-container">
            <table id = "lb-table">
            <thead>
            <th>Product</th><br>
            <th>Quantity</th> 
            <th>Price</th>

            </thead>
            <tr>
            <td></td>
            <td></td>
            <td></td>
            </tr>
            <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            </tr>
            <tr>
            <td></td>
            <td></td>
            <td></td>
            <td> Subtotal</td>
            </tr>
            </table>
            </div>
            </tr>
            </thead><!-- thead Ends -->
            <tbody><!-- tbody Starts -->
            <tr>
            </tbody><!-- tbody Ends -->
            </table><!-- table Ends -->
            </div><!-- table-responsive Ends -->
            <div class="box-footer"><!-- box-footer Starts -->
            <div class="pull-left"><!-- pull-left Starts -->
            <a href="index.php" class="btn btn-default">
            <i class="fa fa-chevron-left"></i> Continue Shopping
            </a>
            </div><!-- pull-left Ends -->
            <div class="pull-right"><!-- pull-right Starts -->
            <button class="btn btn-default" type="submit" name="update" value="Update Cart">
            <i class="fa fa-refresh"></i> Update Cart
            </button>
            <a href="#" class="btn btn-primary">
            Proceed to checkout <i class="fa fa-chevron-right"></i>
            </a>
            </div><!-- pull-right Ends -->
