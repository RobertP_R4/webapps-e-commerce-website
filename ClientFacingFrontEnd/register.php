<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Register Form</title>
    <link rel="stylesheet" href="css/style.css">
</head>
<body>
    <div class="header">
    <div class="container">
    <div class="navbar">
    <div class="logo">
    <img src="images/logo.png" alt="" width="50px">                
</div>
<nav>
    <ul id="menuitem">
        <li><a href="index.php">Home</a></li>
        <li><a href="login.php">Login</a></li>
        <li><a href="register.php">Register</a></li>
        <li><a href="account.php"> My Account</a></li>
        <li><a href="#">Logout</a></li>
        <li><a href="cart.php">Basket</a></li>
</ul>
</nav></div>
</div>
    <div id="content">
    <div class="container">
    <div class="col-md-9">
<!--        WELCOME STRIP-->
			<div class="row">
			<div class="box">
			<h1>Register New Customer</h1>
			<p>
            <a href="login.php" class="btn btn-success btn-sm">Click here to LOGIN.</a>
			</p>
			</div>
			</div>
            <!-- ITEMS-->
            <div class="row">
			<div class="box">
            <form style="border:1px solid #ccc" method='POST'> <!--add POST Action -->
			<div class="container">
                <br>
                <label for="name"><b>First Name</b></label><br>
				<input type="text" placeholder="Enter Name" name="name" required><br>
				
                <label for="name"><b>Last Name</b></label><br>
				<input type="text" placeholder="Enter Name" name="name" required><br>
				
				<label for="email"><b>Email Address</b></label><br>
				<input type="email" placeholder="Enter Email" name="email" required><br>
				
                <label for="DateofBirth"><b>Date of Birth</b></label><br>
				<input type="date" placeholder="Enter date of birth" name="DateofBirth" required><br><br>

				<label for="name"><b>Username</b></label><br>
				<input type="text" placeholder="Enter Name" name="name" required><br>
				
                <label for="password"><b>Password</b></label><br>
				<input type="password" placeholder="Enter Password" name="password" required><br>

				<label for="psw-repeat"><b>Repeat Password</b></label><br>
				<input type="password" placeholder="Repeat Password" name="psw-repeat" required><br>

				<label for="address"><b>Address</b></label><br>
				<input type="text" placeholder="Enter Address" name="address" required><br>
				
				<p>By creating an account you agree to our <a href="#" style="color:dodgerblue">Terms & Privacy</a>.</p>

				<div class="clearfix">
				<button type="submit" class="signupbtn" name="register">Sign Up</button>
				</div>
			    </div>
			    </form>  
			    </div>
            </div>
        </div>
    </div>
</div>
</html>